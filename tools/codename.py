#!/usr/bin/env python
from __future__ import print_function
names = ["aces", "astonishing", "astounding",
    "best", "breathtaking", "cool",
    "doozie", "extravagant", "fab", "fantastic",
    "groovy",
    "immense", "in spades", "inconceivable",
    "legendary", "marvelous", "mind-blowing", "out-of-this-world",
    "outrageous", "phenomenal", "primo", "prodigious",
    "remarkable", "spectacular", "striking",
    "super", "superb", "terrific", "top drawer", "tops", "turn-on",
    "unbelievable", "wicked", "wondrous"]

import random
print(random.choice(names))
