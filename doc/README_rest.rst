TurboRest
=========

TurboRest provides an alternative to the cumbersome RESTMethod bundled with
TurboGears 1.5. It supplies two abstractions


``RESTResource``
    Controller base class that provides HTTP method-based dispatch:

    .. code-block:: python

        class CandidateResource(RESTResource):
            def __init__(self, id):
                super(CandidateResource, self).__init__()
                self.candidate_id = id

            @expose()
            def GET(self):
                return {'success': True, 'candidate': {'id': self.candidate_id}}

            @expose()
            def POST(self):
                """Do POST-y things"""


``RESTContainer``
    Class decorator for creating containers of a specified resource. For
    example, to create a list of candidate resources such that::

        /candidates/<id>

    returns a candidate resource for the specified candidate, define the
    candidates controller as

    .. code-block:: python

        @RESTContainer(CandidateResource)
        class CandidateRootController(Controller):
            pass


    The resource class must have a constructor that takes a single integer ID
    as its parameter.
